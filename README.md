This script compare the Descending Critical Aware Utilization order with the Descending Critical Aware Period order in two response time models for Mixed Criticality Systems: 
partitioned model using Vestal response time
and semi-partitioned model using Xu and Burns response time.

## Requirements

The script run on python3 and needs the following packages:

1. matplotlib
2. scipy

you can use pip3 to install: pip3 install [package-name]

---

## Usage

Run main.py. 

The script will produce two graphics and two csv file, one for each model (partitioned/semi-partitioned)

---

# Configurations

In file tasksetsgeneration.py you can set the following parameters

1. samplesnumber : number of taskset to be tested for each utilization level
2. tasksforeachset : number of tasks for each taskset
3. coresnumber : number of cores of the platform. For now the script only works with 2 core.
4. lperiod, hperiod : periods range for the log-uniform algorithm
5. percent : HI-crit percent of tasks in a taskset
6. factor : multiple rate between HI-crit WCET and LO-crit WCET

In file main.py you can set:

1. utilization : the first sum of utilizations used for the utilization generation. This value increase by 0.028 at each step
until reaching 2.1. 

---